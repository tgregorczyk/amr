module meshing
    use VTR
    use ptr
    use node
    use variables
    use, intrinsic :: iso_fortran_env
    implicit none

    type(Nodes), dimension(n, n), target :: Mesh
    real(real64), dimension(1:2**max_level_refinement, 1:2**max_level_refinement) :: temp_array
    type(pointeur), dimension(1:(n*2**(max_level_refinement))**2) :: leaves
    integer :: number_of_leaves = int((n*2**(max_level_refinement))**2)

    contains

    subroutine init_neighbors()
        integer :: i, j

        do i=1,(n*2**(max_level_refinement))**2
            leaves(i)%p => null()
        end do

        do j = 1,n
            do i = 1,n
                Mesh(i,j)%parent => Mesh(i,j)
            end do
        end do

        do j = 2,n-1
            do i = 2,n-1

                Mesh(i,j)%n => Mesh(i-1,j)
                Mesh(i,j)%s => Mesh(i+1,j)
                Mesh(i,j)%w => Mesh(i,j-1)
                Mesh(i,j)%e => Mesh(i,j+1)

                Mesh(i,j)%nw => Mesh(i-1,j-1)
                Mesh(i,j)%ne => Mesh(i-1,j+1)
                Mesh(i,j)%sw => Mesh(i+1,j-1)
                Mesh(i,j)%se => Mesh(i+1,j+1)
                
            end do
        end do

        do i=2,n-1

            Mesh(i,1)%n => Mesh(i-1,1)
            Mesh(i,1)%s => Mesh(i+1,1)
            Mesh(i,1)%w => Mesh(i,n)
            Mesh(i,1)%e => Mesh(i,2)

            Mesh(i,1)%nw => Mesh(i-1,n)
            Mesh(i,1)%ne => Mesh(i-1,2)
            Mesh(i,1)%sw => Mesh(i+1,n)
            Mesh(i,1)%se => Mesh(i+1,2)

            Mesh(i,n)%n => Mesh(i-1,n)
            Mesh(i,n)%s => Mesh(i+1,n)
            Mesh(i,n)%w => Mesh(i,n-1)
            Mesh(i,n)%e => Mesh(i,1)

            Mesh(i,n)%nw => Mesh(i-1,n-1)
            Mesh(i,n)%ne => Mesh(i-1,1)
            Mesh(i,n)%sw => Mesh(i+1,n-1)
            Mesh(i,n)%se => Mesh(i+1,1)

        end do

        do j = 2,n-1
            Mesh(1,j)%n => Mesh(n,j)
            Mesh(1,j)%s => Mesh(2,j)
            Mesh(1,j)%w => Mesh(1,j-1)
            Mesh(1,j)%e => Mesh(1,j+1)

            Mesh(1,j)%nw => Mesh(n,j-1)
            Mesh(1,j)%ne => Mesh(n,j+1)
            Mesh(1,j)%sw => Mesh(2,j-1)
            Mesh(1,j)%se => Mesh(2,j+1)

            Mesh(n,j)%n => Mesh(n-1,j)
            Mesh(n,j)%s => Mesh(1,j)
            Mesh(n,j)%w => Mesh(n,j-1)
            Mesh(n,j)%e => Mesh(n,j+1)

            Mesh(n,j)%nw => Mesh(n-1,j-1)
            Mesh(n,j)%ne => Mesh(n-1,j+1)
            Mesh(n,j)%sw => Mesh(1,j-1)
            Mesh(n,j)%se => Mesh(1,j+1)

        end do

        Mesh(1,1)%n => Mesh(n,1)
        Mesh(1,1)%s => Mesh(2,1)
        Mesh(1,1)%w => Mesh(1,n)
        Mesh(1,1)%e => Mesh(1,2)

        Mesh(1,1)%nw => Mesh(n,n)
        Mesh(1,1)%ne => Mesh(n,2)
        Mesh(1,1)%sw => Mesh(2,n)
        Mesh(1,1)%se => Mesh(2,2)

        Mesh(1,n)%n => Mesh(n,n)
        Mesh(1,n)%s => Mesh(2,n)
        Mesh(1,n)%w => Mesh(1,n-1)
        Mesh(1,n)%e => Mesh(1,1)

        Mesh(1,n)%nw => Mesh(n,n-1)
        Mesh(1,n)%ne => Mesh(n,1)
        Mesh(1,n)%sw => Mesh(2,n-1)
        Mesh(1,n)%se => Mesh(2,1)

        Mesh(n,1)%n => Mesh(n-1,1)
        Mesh(n,1)%s => Mesh(1,1)
        Mesh(n,1)%w => Mesh(n,n)
        Mesh(n,1)%e => Mesh(n,2)

        Mesh(n,1)%nw => Mesh(n-1,n)
        Mesh(n,1)%ne => Mesh(n-1,2)
        Mesh(n,1)%sw => Mesh(1,n)
        Mesh(n,1)%se => Mesh(1,2)

        Mesh(n,n)%n => Mesh(n-1,n)
        Mesh(n,n)%s => Mesh(1,n)
        Mesh(n,n)%w => Mesh(n,n-1)
        Mesh(n,n)%e => Mesh(n,1)

        Mesh(n,n)%nw => Mesh(n-1,n-1)
        Mesh(n,n)%ne => Mesh(n-1,1)
        Mesh(n,n)%sw => Mesh(1,n-1)
        Mesh(n,n)%se => Mesh(1,1)

    end subroutine init_neighbors


    recursive subroutine to_array(bloc, field, offsetx, offsety)
        type(Nodes), intent(in) :: bloc
        integer, intent(in) :: field
        integer, intent(inout):: offsetx, offsety
        integer :: offsetx_bak, offsety_bak
        integer :: startx, endx, starty, endy

        offsetx_bak = offsetx
        offsety_bak = offsety

        if (bloc%children(1,1)%isLeaf()) then
            startx = offsetx
            endx = offsetx + 2**(max_level_refinement - bloc%children(1,1)%refinement_level) - 1
            starty = offsety
            endy = offsety + 2**(max_level_refinement - bloc%children(1,1)%refinement_level) - 1
            temp_array(startx:endx, starty:endy) = bloc%children(1,1)%values(field)
        else
            call to_array(bloc%children(1,1), field, offsetx, offsety)

        end if

        offsetx = offsetx_bak
        offsety = offsety_bak + 2**(max_level_refinement - bloc%children(1,2)%refinement_level) 
        if (bloc%children(1,2)%isLeaf()) then
            startx = offsetx
            endx = offsetx + 2**(max_level_refinement - bloc%children(1,2)%refinement_level) - 1
            starty = offsety
            endy = offsety + 2**(max_level_refinement - bloc%children(1,2)%refinement_level) - 1
            temp_array(startx:endx, starty:endy) = bloc%children(1,2)%values(field)
        else
            call to_array(bloc%children(1,2), field, offsetx, offsety)
        end if

        offsetx = offsetx_bak + 2**(max_level_refinement - bloc%children(2,1)%refinement_level) 
        offsety = offsety_bak
        if (bloc%children(2,1)%isLeaf()) then
            startx = offsetx
            endx = offsetx + 2**(max_level_refinement - bloc%children(2,1)%refinement_level) - 1
            starty = offsety
            endy = offsety + 2**(max_level_refinement - bloc%children(2,1)%refinement_level) - 1
            temp_array(startx:endx, starty:endy) = bloc%children(2,1)%values(field)
        else
            call to_array(bloc%children(2,1), field, offsetx, offsety)
        end if

        offsetx = offsetx_bak + 2**(max_level_refinement - bloc%children(2,2)%refinement_level) 
        offsety = offsety_bak + 2**(max_level_refinement - bloc%children(2,2)%refinement_level) 
        if (bloc%children(2,2)%isLeaf()) then
            startx = offsetx
            endx = offsetx + 2**(max_level_refinement - bloc%children(2,2)%refinement_level) - 1
            starty = offsety
            endy = offsety + 2**(max_level_refinement - bloc%children(2,2)%refinement_level) - 1
            temp_array(startx:endx, starty:endy) = bloc%children(2,2)%values(field)
        else
            call to_array(bloc%children(2,2), field, offsetx, offsety)
        end if

    end subroutine to_array

    subroutine array_to_output(val, field)
        real(real64), dimension(1:n*2**max_level_refinement,1:n*2**max_level_refinement), intent(out) :: val
        integer, intent(in) :: field
        integer :: offsetx, offsety
        integer :: i,j

        do j=1,n
            do i=1,n
                if (Mesh(i,j)%isLeaf()) then
                    temp_array(:,:) = Mesh(i,j)%values(field)
                else
                    offsetx = 1
                    offsety = 1
                    call to_array(Mesh(i,j), field, offsetx, offsety)

                end if

                val((i - 1) * 2**max_level_refinement + 1: i * 2**max_level_refinement, &
                    (j - 1) * 2**max_level_refinement + 1: j * 2**max_level_refinement ) = temp_array
                
            end do
        end do

    end subroutine array_to_output

    recursive subroutine rec_to_map(bloc, offsetx, offsety)
        type(Nodes), intent(in) :: bloc
        integer, intent(inout):: offsetx, offsety
        integer :: offsetx_bak, offsety_bak
        integer :: startx, endx, starty, endy

        offsetx_bak = offsetx
        offsety_bak = offsety

        if (bloc%children(1,1)%isLeaf()) then
            startx = offsetx
            endx = offsetx + 2**(max_level_refinement - bloc%children(1,1)%refinement_level) - 1
            starty = offsety
            endy = offsety + 2**(max_level_refinement - bloc%children(1,1)%refinement_level) - 1
            temp_array(startx:endx, starty:endy) = bloc%children(1,1)%refinement_level
        else
            call rec_to_map(bloc%children(1,1), offsetx, offsety)

        end if

        offsetx = offsetx_bak
        offsety = offsety_bak + 2**(max_level_refinement - bloc%children(1,2)%refinement_level) 
        if (bloc%children(1,2)%isLeaf()) then
            startx = offsetx
            endx = offsetx + 2**(max_level_refinement - bloc%children(1,2)%refinement_level) - 1
            starty = offsety
            endy = offsety + 2**(max_level_refinement - bloc%children(1,2)%refinement_level) - 1
            temp_array(startx:endx, starty:endy) = bloc%children(1,2)%refinement_level
        else
            call rec_to_map(bloc%children(1,2), offsetx, offsety)
        end if

        offsetx = offsetx_bak + 2**(max_level_refinement - bloc%children(2,1)%refinement_level) 
        offsety = offsety_bak
        if (bloc%children(2,1)%isLeaf()) then
            startx = offsetx
            endx = offsetx + 2**(max_level_refinement - bloc%children(2,1)%refinement_level) - 1
            starty = offsety
            endy = offsety + 2**(max_level_refinement - bloc%children(2,1)%refinement_level) - 1
            temp_array(startx:endx, starty:endy) = bloc%children(2,1)%refinement_level
        else
            call rec_to_map(bloc%children(2,1), offsetx, offsety)
        end if

        offsetx = offsetx_bak + 2**(max_level_refinement - bloc%children(2,2)%refinement_level) 
        offsety = offsety_bak + 2**(max_level_refinement - bloc%children(2,2)%refinement_level) 
        if (bloc%children(2,2)%isLeaf()) then
            startx = offsetx
            endx = offsetx + 2**(max_level_refinement - bloc%children(2,2)%refinement_level) - 1
            starty = offsety
            endy = offsety + 2**(max_level_refinement - bloc%children(2,2)%refinement_level) - 1
            temp_array(startx:endx, starty:endy) = bloc%children(2,2)%refinement_level
        else
            call rec_to_map(bloc%children(2,2), offsetx, offsety)
        end if

    end subroutine rec_to_map

    subroutine create_map_refinement(map_refinement)
        real(real64), dimension(1:n*2**max_level_refinement,1:n*2**max_level_refinement), intent(out) :: map_refinement
        integer :: offsetx, offsety
        integer :: i,j

        do j=1,n
            do i=1,n
                if (Mesh(i,j)%isLeaf()) then
                    temp_array(:,:) = Mesh(i,j)%refinement_level
                else

                    offsetx = 1
                    offsety = 1

                    call rec_to_map(Mesh(i,j), offsetx, offsety)

                end if

                map_refinement((i - 1) * 2**max_level_refinement + 1: i * 2**max_level_refinement, &
                          (j - 1) * 2**max_level_refinement + 1: j * 2**max_level_refinement ) = temp_array
                
            end do
        end do

    end subroutine create_map_refinement


    subroutine make_vtr(x, y, fd)
        real(real64), intent(in) :: x(:), y(:)
        real(real64), dimension(1:n*2**max_level_refinement,1:n*2**max_level_refinement) :: val, map_refinement
        type(VTR_file_handle), intent(inout) :: fd

        call VTR_open_file(PREFIX="AMR", FD=fd)
        call VTR_write_mesh(FD=fd, X=x, Y=y)

        call array_to_output(val, phi)
        call VTR_write_var(FD=fd, NAME="Phi", FIELD=val)

        call array_to_output(val, rho)
        call VTR_write_var(FD=fd, NAME="rho", FIELD=val)

        call array_to_output(val, ux)
        call VTR_write_var(FD=fd, NAME="ux", FIELD=val)

        call array_to_output(val, uy)
        call VTR_write_var(FD=fd, NAME="uy", FIELD=val)

        call array_to_output(val, p)
        call VTR_write_var(FD=fd, NAME="p", FIELD=val)

        call create_map_refinement(map_refinement)
        call VTR_write_var(FD=fd, NAME="refinement", FIELD=map_refinement)

        call VTR_close_file(FD=fd)

    end subroutine make_vtr

    subroutine inv_neighbor(b, a, ba)
        type(Nodes), intent(in), target :: b
        type(Nodes), pointer, intent(out) :: ba
        integer, intent(in) :: a

        select case(a)
            case (1)
            ba => b

            case (2)
            ba => b%w

            case (3) 
            ba => b%s

            case (4) 
            ba => b%e

            case (5)
            ba => b%n

            case (6) 
            ba => b%sw

            case (7) 
            ba => b%se

            case (8) 
            ba => b%ne

            case (9) 
            ba => b%nw
        end select

    end subroutine inv_neighbor

    subroutine neighbor(b, a, ba)
        type(Nodes), intent(in), target :: b
        type(Nodes), pointer, intent(out) :: ba
        integer, intent(in) :: a

        select case(a)
            case (1)
            ba => b

            case (2)
            ba => b%e

            case (3) 
            ba => b%n

            case (4) 
            ba => b%w

            case (5)
            ba => b%s

            case (6) 
            ba => b%ne

            case (7) 
            ba => b%nw

            case (8) 
            ba => b%sw

            case (9) 
            ba => b%se
        end select

    end subroutine neighbor

    subroutine neighborx(b, a, ba)
        type(Nodes), intent(in), target :: b
        type(Nodes), pointer, intent(out) :: ba
        integer, intent(in) :: a

        select case(a)
            case (1)
            ba => b

            case (2)
            ba => b%e

            case (3) 
            ba => b

            case (4) 
            ba => b%w

            case (5)
            ba => b

            case (6) 
            ba => b%e

            case (7) 
            ba => b%w

            case (8) 
            ba => b%w

            case (9) 
            ba => b%e
        end select

    end subroutine neighborx


    subroutine neighbory(b, a, ba)
        type(Nodes), intent(in), target :: b
        type(Nodes), pointer, intent(out) :: ba
        integer, intent(in) :: a

        select case(a)
            case (1)
            ba => b

            case (2)
            ba => b

            case (3) 
            ba => b%n

            case (4) 
            ba => b

            case (5)
            ba => b%s

            case (6) 
            ba => b%n

            case (7) 
            ba => b%n

            case (8) 
            ba => b%s

            case (9) 
            ba => b%s
        end select

    end subroutine neighbory


    recursive subroutine check_refinement(point)
        type(Nodes), intent(inout), target :: point
        real(real64) :: normdPhi

        if(point%isLeaf()) then

            leaves(number_of_leaves + 1)%p => point
            number_of_leaves = number_of_leaves + 1

            ! normdPhi = sqrt(point%values(dPhix)**2 + point%values(dPhiy)**2)
            ! if (normdPhi > 5.0d-3) then
            !     call point%refine
            ! else if (normdPhi < 1.0d-3) then
            !     call point%coarsen
            ! end if

            ! ! one level of refinement
            ! if ( (abs(point%values(Phi)) < 0.99) )then
            !     if (point%refinement_level .EQ. 0) then
            !         call point%refine
            !     end if
            ! else 
            !     if (point%refinement_level .EQ. 1) then
            !         call point%parent%coarsen
            !     end if
            ! end if

            ! ! two levels of refinement / no derefinement
            ! if ( (abs(point%values(Phi)) < 0.99) )then
            !     if (point%refinement_level .LE. 1) then
            !         call point%refine
            !     end if
            ! else 
            !     if (point%refinement_level .EQ. 2) then
            !         call point%parent%coarsen
            !     end if
            ! end if

            ! ! three levels of refinement / no derefinement
            ! if ( (abs(point%values(Phi)) < 0.99) )then
            !     if (point%refinement_level .LE. 2) then
            !         call point%refine
            !     end if
            ! else 
            !     if (point%refinement_level .GE. 1) then
            !         call point%parent%coarsen
            !     end if
            ! end if


            ! ! two levels of refinement
            ! if ( (abs(point%values(Phi)) < 0.9999999) .AND. (abs(point%values(Phi)) > 0.9) )then
            !     if (point%refinement_level .EQ. 0) then
            !         call point%refine
            !     else if (point%refinement_level > 1) then
            !         call point%parent%coarsen
            !     end if
            ! else if ( abs(point%values(Phi)) .LE. 0.9 ) then
            !     if (point%refinement_level .LE. 1) then
            !         call point%refine
            !     end if
            ! else if (abs(point%values(Phi)) .GE. 0.9999999) then
            !     if (point%refinement_level > 0) then
            !         call point%parent%coarsen
            !     end if
            ! end if

            ! ! three levels of refinement
            ! if ( (abs(point%values(Phi)) < 0.9999999) .AND. (abs(point%values(phi)) > 0.99999) )then
            !     if (point%refinement_level < 1) then
            !         call point%refine
            !     else if (point%refinement_level > 1) then
            !         call point%parent%coarsen
            !     end if
            ! else if ( (abs(point%values(Phi)) < 0.99999) .AND. (abs(point%values(phi)) > 0.9) )then
            !     if (point%refinement_level < 2) then
            !         call point%refine
            !     else if (point%refinement_level > 2) then
            !         call point%parent%coarsen
            !     end if
            ! else if ( abs(point%values(Phi)) .LE. 0.9 ) then
            !     if (point%refinement_level .LE. 2) then
            !         call point%refine
            !     end if
            ! else if (abs(point%values(Phi)) .GE. 0.9999999) then
            !     if (point%refinement_level > 0) then
            !         call point%parent%coarsen
            !     end if
            ! end if

            ! ! three levels of refinement
            ! if ( point%values(Phi) > -0.9999999 .AND. point%values(phi) < -0.99999 )then
            !     if (point%refinement_level < 1) then
            !         call point%refine
            !     else if (point%refinement_level > 1) then
            !         call point%parent%coarsen
            !     end if
            ! else if ( (point%values(Phi)) > -0.99999 .AND. point%values(phi) < -0.9 )then
            !     if (point%refinement_level < 2) then
            !         call point%refine
            !     else if (point%refinement_level > 2) then
            !         call point%parent%coarsen
            !     end if
            ! else if ( point%values(Phi) .LE. -0.9 ) then
            !     if (point%refinement_level .LE. 2) then
            !         call point%refine
            !     end if
            ! else if (point%values(Phi) .LE. -0.9999999) then
            !     if (point%refinement_level > 0) then
            !         call point%parent%coarsen
            !     end if
            ! end if


        else
            if (.not. point%hasGhosts()) then
                call check_refinement(point%children(1,1))
            end if
            if (.not. point%hasGhosts()) then
                call check_refinement(point%children(1,2))
            end if
            if (.not. point%hasGhosts()) then
                call check_refinement(point%children(2,1))
            end if
            if (.not. point%hasGhosts()) then
                call check_refinement(point%children(2,2))
            end if

        end if
            
    end subroutine check_refinement

    subroutine deallocate_all_children()
        integer :: i, j

        do j = 1,n
            do i = 1,n
                call Mesh(i,j)%deallocate_children
            end do
        end do

    end subroutine deallocate_all_children

    subroutine compute_mesh()
        integer :: i, j

        ! NOTE: we will update leaves here:
        ! we first make everything point to null()
        ! then each time we find leaf we make a leaves(i) point to it
        ! we also count the number of leaves so that we won't have to loop
        ! over the whole array every time

        do i = 1,(n*2**(max_level_refinement))**2
            leaves(i)%p => null()
        end do
        number_of_leaves = 0

        ! WARNING: parallelization messes everything up
        ! maybe number_of_leaves should be shared or something

        !!$omp parallel do
        do j = 1,n
            do i = 1,n
                call check_refinement(Mesh(i,j))
            end do
        end do
        !!$omp end parallel do

    end subroutine compute_mesh

end module meshing
