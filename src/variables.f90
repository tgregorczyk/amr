module variables
    use, intrinsic :: iso_fortran_env
    implicit none
    
    integer, parameter :: n = 150, max_level_refinement = 1, max_iter = 20000, save_vtr = 1000
    integer, parameter :: latticeSize = 9
    real(real64) :: density_ratio = 2.0d0, cs=1.0d0/sqrt(3.0d0), sigma = 1.0d-2, rho_0 = 1.0d0
    real(real64) :: nu1 = 1.0d-5, nu2 = 1.0d-5, W = 2.0d0, epsilon = 1.0d-20, c_pressure = 1.0d-1
    integer, dimension(0:max_level_refinement) :: count_refinement

    real(real64), dimension(1:latticeSize) :: weight, ex, ey, Hxx, Hxy, Hyy, Hxyy, Hyxx, Hxxyy

end module variables
