module ptr
    use variables
    use node
    use, intrinsic :: iso_fortran_env
    implicit none

    !this is needed to create an array of pointers
    type pointeur
    type(Nodes), pointer :: p

    end type pointeur

end module ptr
