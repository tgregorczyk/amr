module node
    use variables
    use, intrinsic :: iso_fortran_env
    implicit none

    integer :: highest_refinement = 0
    integer, parameter :: nb_values = 25, phi = 1, rho = 2, ux = 3, uy = 4, p = 5
    integer, parameter :: f0 = 6, f1 = 7, f2 = 8, f3 = 9, f4 = 10, f5 = 11, f6 = 12, f7 = 13, f8 = 14
    integer, parameter :: f0col = 15, f1col = 16, f2col = 17, f3col = 18, f4col = 19,&
    f5col = 20, f6col = 21, f7col = 22, f8col = 23, dPhix = 24, dPhiy = 25

    type Nodes
        integer :: refinement_level = 0
        real(real64) :: values(nb_values) = 0.0d0
        type(Nodes), pointer :: n => null()
        type(Nodes), pointer :: s => null()
        type(Nodes), pointer :: e => null()
        type(Nodes), pointer :: w => null()
        type(Nodes), pointer :: nw => null()
        type(Nodes), pointer :: sw => null()
        type(Nodes), pointer :: ne => null()
        type(Nodes), pointer :: se => null()
        type(Nodes), pointer :: parent => null()
        type(Nodes), dimension(:, :), allocatable :: children

        contains

        procedure :: hasGhosts => hasGhosts
        procedure :: isLeaf => isLeaf
        procedure :: refine => refine
        procedure :: coarsen => coarsen
        procedure :: deallocate_children => deallocate_children
        procedure :: update_value_from_children => update_value_from_children
        procedure :: update_value_from_parent => update_value_from_parent
        procedure :: print_info => print_info
        procedure :: create_fine_from_coarse => create_fine_from_coarse
        procedure :: create_ghosts => create_ghosts
        procedure :: link_refined_children => link_refined_children
        procedure :: update => update

    end type Nodes

    contains

    pure function hasGhosts(this) result(bool)
        logical :: bool
        Class(Nodes), intent(in), target :: this

        bool = .FALSE.

        if ((allocated(this%children)) .AND. (this%refinement_level .EQ. this%children(1,1)%refinement_level)) then
            bool = .TRUE.
        end if

    end function hasGhosts

    pure function isLeaf(this) result(bool)
        logical :: bool
        Class(Nodes), intent(in), target :: this

        bool = .FALSE.

        if ((.not. allocated(this%children)) .OR. this%hasGhosts()) then
            bool = .TRUE.
        end if 
        
    end function isLeaf

    subroutine create_fine_from_coarse(this)
        Class(Nodes), intent(inout), target :: this

        this%children(1,1) = Nodes(this%refinement_level + 1)
        this%children(1,2) = Nodes(this%refinement_level + 1)
        this%children(2,1) = Nodes(this%refinement_level + 1)
        this%children(2,2) = Nodes(this%refinement_level + 1)

        this%children(1,1)%parent => this
        this%children(1,2)%parent => this
        this%children(2,1)%parent => this
        this%children(2,2)%parent => this

        if (this%refinement_level + 1 > highest_refinement) then
            highest_refinement = this%refinement_level + 1
        end if

        ! we set the value of the children
        call this%update_value_from_parent

    end subroutine create_fine_from_coarse

    subroutine create_ghosts(this)
        Class(Nodes), intent(inout), target :: this

        if (.not. allocated(this%w%children)) then
            allocate(this%w%children(1:2,1:2))
            call this%w%create_fine_from_coarse

            !we don't want the neighboring node to be refined! So the fact that its refinement_level stays the same
            ! indicates that children are only ghosts
            this%w%children(1,1)%refinement_level = this%w%children(1,1)%refinement_level - 1
            this%w%children(1,2)%refinement_level = this%w%children(1,2)%refinement_level - 1
            this%w%children(2,1)%refinement_level = this%w%children(2,1)%refinement_level - 1
            this%w%children(2,2)%refinement_level = this%w%children(2,2)%refinement_level - 1

        end if


        if (.not. allocated(this%e%children)) then
            allocate(this%e%children(1:2,1:2))
            call this%e%create_fine_from_coarse

            this%e%children(1,1)%refinement_level = this%e%children(1,1)%refinement_level - 1
            this%e%children(1,2)%refinement_level = this%e%children(1,2)%refinement_level - 1
            this%e%children(2,1)%refinement_level = this%e%children(2,1)%refinement_level - 1
            this%e%children(2,2)%refinement_level = this%e%children(2,2)%refinement_level - 1

        end if

        if (.not. allocated(this%n%children)) then
            allocate(this%n%children(1:2,1:2))
            call this%n%create_fine_from_coarse

            this%n%children(1,1)%refinement_level = this%n%children(1,1)%refinement_level - 1
            this%n%children(1,2)%refinement_level = this%n%children(1,2)%refinement_level - 1
            this%n%children(2,1)%refinement_level = this%n%children(2,1)%refinement_level - 1
            this%n%children(2,2)%refinement_level = this%n%children(2,2)%refinement_level - 1

        end if

        if (.not. allocated(this%s%children)) then
            allocate(this%s%children(1:2,1:2))
            call this%s%create_fine_from_coarse

            this%s%children(1,1)%refinement_level = this%s%children(1,1)%refinement_level - 1
            this%s%children(1,2)%refinement_level = this%s%children(1,2)%refinement_level - 1
            this%s%children(2,1)%refinement_level = this%s%children(2,1)%refinement_level - 1
            this%s%children(2,2)%refinement_level = this%s%children(2,2)%refinement_level - 1

        end if

        !diagonal neighbors

        if (.not. allocated(this%se%children)) then
            allocate(this%se%children(1:2,1:2))
            call this%se%create_fine_from_coarse

            this%se%children(1,1)%refinement_level = this%se%children(1,1)%refinement_level - 1
            this%se%children(1,2)%refinement_level = this%se%children(1,2)%refinement_level - 1
            this%se%children(2,1)%refinement_level = this%se%children(2,1)%refinement_level - 1
            this%se%children(2,2)%refinement_level = this%se%children(2,2)%refinement_level - 1

        end if

        if (.not. allocated(this%ne%children)) then
            allocate(this%ne%children(1:2,1:2))
            call this%ne%create_fine_from_coarse

            this%ne%children(1,1)%refinement_level = this%ne%children(1,1)%refinement_level - 1
            this%ne%children(1,2)%refinement_level = this%ne%children(1,2)%refinement_level - 1
            this%ne%children(2,1)%refinement_level = this%ne%children(2,1)%refinement_level - 1
            this%ne%children(2,2)%refinement_level = this%ne%children(2,2)%refinement_level - 1

        end if

        if (.not. allocated(this%sw%children)) then
            allocate(this%sw%children(1:2,1:2))
            call this%sw%create_fine_from_coarse

            this%sw%children(1,1)%refinement_level = this%sw%children(1,1)%refinement_level - 1
            this%sw%children(1,2)%refinement_level = this%sw%children(1,2)%refinement_level - 1
            this%sw%children(2,1)%refinement_level = this%sw%children(2,1)%refinement_level - 1
            this%sw%children(2,2)%refinement_level = this%sw%children(2,2)%refinement_level - 1

        end if

        if (.not. allocated(this%nw%children)) then
            allocate(this%nw%children(1:2,1:2))
            call this%nw%create_fine_from_coarse

            this%nw%children(1,1)%refinement_level = this%nw%children(1,1)%refinement_level - 1
            this%nw%children(1,2)%refinement_level = this%nw%children(1,2)%refinement_level - 1
            this%nw%children(2,1)%refinement_level = this%nw%children(2,1)%refinement_level - 1
            this%nw%children(2,2)%refinement_level = this%nw%children(2,2)%refinement_level - 1

        end if

    end subroutine create_ghosts

    subroutine link_refined_children(this)
        Class(Nodes), intent(inout), target :: this

            this%children(1,1)%w => this%w%children(1,2)
            this%w%children(1,2)%e => this%children(1,1)
            this%children(2,1)%w => this%w%children(2,2)
            this%w%children(2,2)%e => this%children(2,1)

            this%children(1,2)%e => this%e%children(1,1)
            this%e%children(1,1)%w => this%children(1,2)
            this%children(2,2)%e => this%e%children(2,1)
            this%e%children(2,1)%e => this%children(2,2)

            this%children(1,1)%n => this%n%children(2,1)
            this%n%children(2,1)%s => this%children(1,1)
            this%children(1,2)%n => this%n%children(2,1)
            this%n%children(2,1)%s => this%children(1,2)

            this%children(2,1)%s => this%s%children(1,1)
            this%s%children(1,1)%n => this%children(2,1)
            this%children(2,2)%s => this%s%children(1,2)
            this%s%children(1,2)%n => this%children(2,2)

            this%children(1,1)%e => this%children(1,2)
            this%children(1,1)%s => this%children(2,1)

            this%children(1,2)%w => this%children(1,1)
            this%children(1,2)%s => this%children(2,2)

            this%children(2,1)%e => this%children(2,2)
            this%children(2,1)%n => this%children(1,1)

            this%children(2,2)%w => this%children(2,1)
            this%children(2,2)%n => this%children(1,2)

            this%w%children(1,2)%w => this%w%children(1,1)
            this%w%children(2,2)%w => this%w%children(2,1)
            this%e%children(1,1)%e => this%e%children(1,2)
            this%e%children(2,1)%e => this%e%children(2,2)
            this%n%children(2,1)%n => this%n%children(1,1)
            this%n%children(2,2)%n => this%n%children(1,2)
            this%s%children(1,1)%s => this%s%children(2,1)
            this%s%children(1,2)%s => this%s%children(2,2)

            ! diagonal neighbors

            this%children(1,1)%nw => this%n%children(2,2)
            this%children(1,1)%ne => this%ne%children(2,2)
            this%children(1,1)%sw => this%children(2,2)
            this%children(1,1)%se => this%e%children(2,2)

            this%children(1,2)%nw => this%nw%children(2,1)
            this%children(1,2)%ne => this%n%children(2,1)
            this%children(1,2)%sw => this%w%children(2,1)
            this%children(1,2)%se => this%children(2,1)

            this%children(2,1)%nw => this%children(1,2)
            this%children(2,1)%ne => this%e%children(1,2)
            this%children(2,1)%sw => this%s%children(1,2)
            this%children(2,1)%se => this%se%children(1,2)

            this%children(2,2)%nw => this%e%children(1,1)
            this%children(2,2)%ne => this%children(1,1)
            this%children(2,2)%sw => this%sw%children(1,1)
            this%children(2,2)%se => this%s%children(1,1)

            this%se%children(1,1)%se => this%se%children(2,2)
            this%sw%children(1,2)%sw => this%sw%children(1,2)
            this%ne%children(2,1)%ne => this%ne%children(1,2)
            this%nw%children(2,2)%nw => this%nw%children(1,1)

    end subroutine link_refined_children

    subroutine refine(this)
        Class(Nodes), intent(inout), target :: this

        !test if leaf
        if (this%refinement_level < max_level_refinement) then

            ! test if refinement level of neighbors is the same of one time higher
            if ((((this%w%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                ((this%w%refinement_level - this%refinement_level) .EQ. 1)) .AND. &
                (((this%e%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                ((this%e%refinement_level - this%refinement_level) .EQ. 1)) .AND. &
                (((this%n%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                ((this%n%refinement_level - this%refinement_level) .EQ. 1)) .AND. &
                (((this%s%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                ((this%s%refinement_level - this%refinement_level) .EQ. 1)) .AND. &!) then 
                ! we might need to enforce this also for diagonal neighbors
                (((this%se%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                ((this%se%refinement_level - this%refinement_level) .EQ. 1)) .AND. &
                (((this%ne%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                ((this%ne%refinement_level - this%refinement_level) .EQ. 1)) .AND. &
                (((this%sw%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                ((this%sw%refinement_level - this%refinement_level) .EQ. 1)) .AND. &
                (((this%nw%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                ((this%nw%refinement_level - this%refinement_level) .EQ. 1))) then

                if (.NOT. allocated(this%children)) then

                    allocate(this%children(1:2,1:2))

                    ! we bind this%children to new Nodes with correct values and refinement_level
                    call this%create_fine_from_coarse

                    ! if we created an interface we need to create ghost Nodes
                    call this%create_ghosts

                    ! now we just link children to their neighbors
                    call this%link_refined_children

                    ! four new fine nodes, one less coarse node
                    count_refinement(this%refinement_level) = count_refinement(this%refinement_level) - 1
                    count_refinement(this%refinement_level+1) = count_refinement(this%refinement_level+1) + 4

                    ! test if ghost node
                else if (this%hasGhosts()) then

                    ! we update the children values
                    call this%update_value_from_parent

                    ! we change the refinement_level of children so that they are no longer ghosts
                    this%children(1,1)%refinement_level = this%children(1,1)%refinement_level + 1
                    this%children(1,2)%refinement_level = this%children(1,2)%refinement_level + 1
                    this%children(2,1)%refinement_level = this%children(2,1)%refinement_level + 1
                    this%children(2,2)%refinement_level = this%children(2,2)%refinement_level + 1

                    ! if we created an interface we need to create ghost Nodes
                    call this%create_ghosts

                    ! now we just link children to their neighbors
                    call this%link_refined_children

                    ! four new fine nodes, one less coarse node
                    count_refinement(this%refinement_level) = count_refinement(this%refinement_level) - 1
                    count_refinement(this%refinement_level+1) = count_refinement(this%refinement_level+1) + 4

                ! else
                    ! write(*,*) "not a leaf, cannot refine"
                end if
            end if
    end if

    end subroutine refine


    subroutine coarsen(this)
        Class(Nodes), intent(inout), target :: this

        !if refined
        if (allocated(this%children) .AND. .not. this%hasGhosts()) then
            !if children are leaves
            if (this%children(1,1)%isLeaf() .AND. &
                this%children(1,2)%isLeaf() .AND. &
                this%children(2,1)%isLeaf() .AND. &
                this%children(2,2)%isLeaf()) then
                !if neighbors have the same level of refinement or one less
                if ((((this%w%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                    ((this%w%refinement_level - this%refinement_level) .EQ. -1)) .AND. &
                    (((this%e%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                    ((this%e%refinement_level - this%refinement_level) .EQ. -1)) .AND. &
                    (((this%n%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                    ((this%n%refinement_level - this%refinement_level) .EQ. -1)) .AND. &
                    (((this%s%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                    ((this%s%refinement_level - this%refinement_level) .EQ. -1)) .AND. & !) then
                    ! for diagonal neighbors
                    (((this%e%s%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                    ((this%e%s%refinement_level - this%refinement_level) .EQ. -1)) .AND. &
                    (((this%e%n%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                    ((this%e%n%refinement_level - this%refinement_level) .EQ. -1)) .AND. &
                    (((this%w%s%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                    ((this%w%s%refinement_level - this%refinement_level) .EQ. -1)) .AND. &
                    (((this%w%n%refinement_level - this%refinement_level) .EQ. 0) .OR. &
                    ((this%w%n%refinement_level - this%refinement_level) .EQ. -1))) then

                    call this%update_value_from_children

                    this%children(1,1)%refinement_level = this%refinement_level
                    this%children(1,2)%refinement_level = this%refinement_level
                    this%children(2,1)%refinement_level = this%refinement_level
                    this%children(2,2)%refinement_level = this%refinement_level

                    count_refinement(this%refinement_level) = count_refinement(this%refinement_level) + 1
                    count_refinement(this%refinement_level+1) = count_refinement(this%refinement_level+1) - 4

                    ! TODO: create rules to safely deallocate
                    ! deallocate(this%children)

                else
                    write(*,*) "at least one neighbor is more refined"

                end if 
            ! else
            !     write(*,*) "children aren't leaves, cannot coarsen"
            end if
        else
            ! write(*,*) "not refined, cannot coarsen"
        end if

    end subroutine coarsen

    subroutine update_value_from_children(this)
        Class(Nodes), intent(inout), target :: this
        integer :: i

        if (allocated(this%children)) then
            ! TODO: first approximation
            do i=1, nb_values
                this%values(i) = (this%children(1,1)%values(i) + this%children(1,2)%values(i) +&
                this%children(2,1)%values(i) + this%children(2,2)%values(i)) / 4.0
            end do
        end if

    end subroutine update_value_from_children

    subroutine update_value_from_parent(this)
        Class(Nodes), intent(inout), target :: this
        integer :: i
        real(real64) :: P1, P2, P3

        ! TODO: first approximation
        if (allocated(this%children)) then
            do i=1, nb_values

                ! see yuan 2017: An adaptive mesh refinement-multihase LB flux solver
                P1 = (5.0d0 * this%nw%values(i) + 30.0d0 * this%n%values(i) - 3.0d0 * this%ne%values(i)) / 32.0d0
                P2 = (5.0d0 * this%w%values(i) + 30.0d0 * this%values(i) - 3.0d0 * this%e%values(i)) / 32.0d0
                P3 = (5.0d0 * this%sw%values(i) + 30.0d0 * this%s%values(i) - 3.0d0 * this%se%values(i)) / 32.0d0
                this%children(1,1)%values(i) = (5.0d0 * P1 + 30.0d0 * P2 - 3.0d0 * P3) / 32.0d0
                this%children(2,1)%values(i) = (5.0d0 * P3 + 30.0d0 * P2 - 3.0d0 * P1) / 32.0d0
                P1 = (5.0d0 * this%ne%values(i) + 30.0d0 * this%n%values(i) - 3.0d0 * this%nw%values(i)) / 32.0d0
                P2 = (5.0d0 * this%e%values(i) + 30.0d0 * this%values(i) - 3.0d0 * this%w%values(i)) / 32.0d0
                P3 = (5.0d0 * this%se%values(i) + 30.0d0 * this%s%values(i) - 3.0d0 * this%sw%values(i)) / 32.0d0
                this%children(1,2)%values(i) = (5.0d0 * P1 + 30.0d0 * P2 - 3.0d0 * P3) / 32.0d0
                this%children(2,2)%values(i) = (5.0d0 * P3 + 30.0d0 * P2 - 3.0d0 * P1) / 32.0d0

                ! this%children(1,1)%values(i) = 3.0 / 4.0 * this%values(i) &
                ! + 1.0 / 8.0 * (this%w%values(i) + this%n%values(i))
                ! this%children(1,2)%values(i) = 3.0 / 4.0 * this%values(i) &
                ! + 1.0 / 8.0 * (this%e%values(i) + this%n%values(i))
                ! this%children(2,2)%values(i) = 3.0 / 4.0 * this%values(i) &
                ! + 1.0 / 8.0 * (this%e%values(i) + this%s%values(i))
                ! this%children(2,1)%values(i) = 3.0 / 4.0 * this%values(i) &
                ! + 1.0 / 8.0 * (this%w%values(i) + this%s%values(i))

            end do
        end if

    end subroutine update_value_from_parent

    subroutine print_info(this)
        Class(Nodes), intent(in), target :: this

        write(*,*) "refinement_level", this%refinement_level
            
    end subroutine print_info


    subroutine deallocate_children(this)
        Class(Nodes), intent(inout), target :: this

        if (allocated(this%children)) then
            deallocate(this%children)
        end if

    end subroutine deallocate_children


    subroutine update(this)
        Class(Nodes), intent(inout), target :: this

        ! if isLeaf
        ! we need to update to parent value (as it can be the neighbour of a less refined node)
        ! if hasGhosts
        ! same thing + we need to update the children
        if (this%refinement_level .GE. 1) then
            call this%parent%update_value_from_children
        end if

        if (this%hasGhosts()) then
            call this%update_value_from_parent
        end if
    end subroutine update

end module node
