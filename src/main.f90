! TODO: start by refining to the max level, then initialize the values then compute_mesh will coarsen what is needed
! TODO: use VTK instead of VTR
! TODO: better interpolation formulae
! TODO: strange behaviour with two levels of refinement => interface 1/2 change interpolation
! TODO: LBM: not sure if correctly implemented, result change when refinement is used

! NOTE: to deallocate ghosts: 
! - one option is to not deallocate them at all which means we will have to update the value of the children
! each time we update the value of the parents
! - the other option is to deallocate them which means looping through the array
! and if a node with ghost cells has no neighbors more refined than it, we can safely deallocate
! -> right now i don't know what is faster / better

program AMR
    use OMP_LIB
    use VTR
    use variables
    use node
    use ptr
    use meshing
    use LBM
    use, intrinsic :: iso_fortran_env
    implicit none

    type(VTR_file_handle) :: fd
    integer :: i, j, t
    real(real64), dimension(1:n*int(2**max_level_refinement)) :: x, y
    real(real64) :: start_time, end_time
    ! real(real64) :: start_time1, end_time1
    ! logical :: test = .false.

    start_time = omp_get_wtime()
    ! call cpu_time(start_time)

    call init_neighbors()

    call init_values()

    do i=1,n*int(2**max_level_refinement)
        x(i) = i
    end do
    do j=1,n*int(2**max_level_refinement)
        y(j) = j
    end do

    ! do i=1,n
    !     do j=1,n
    !         call Mesh(i,j)%refine
    !     end do
    ! end do

    call compute_mesh()
    call make_vtr(x, y, fd)

    do t=1,max_iter

        call compute_mesh()

        call do_computation()

        if (mod(t, save_vtr) .EQ. 0) then
            write(*,*) "------------------------------------------------------"
            write(*,*) "iteration", t, " / ", max_iter
            do i=0,max_level_refinement
                write(*,*) "nb of points with refinement", i, ":", count_refinement(i)
            end do
            write(*,*) "nb of points", sum(count_refinement)
            call make_vtr(x, y, fd)
        end if

    end do

    call make_vtr(x, y, fd)

    call VTR_collect_file(fd)

    end_time = omp_get_wtime()
    ! call cpu_time(end_time)

    write(*,*) "Finished in", end_time - start_time, "s"

    ! call deallocate_all_children(array)

end program AMR
