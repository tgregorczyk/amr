module LBM
    use VTR
    use node
    use ptr
    use variables
    use meshing
    use, intrinsic :: iso_fortran_env
    implicit none

    contains

    subroutine init_values()
        integer :: i, j
        ! real(real64) :: r = n / 7.0, ux0 = - 1.0d-2, uy0 = 0.0d0
        real(real64) :: r = n / 7.0, ux0 = 0.0d0, uy0 = -1.0d-2
        ! real(real64) :: r = n / 7.0, ux0 = 0.0d0, uy0 = 0.0d0
        real(real64) :: size_of_fluid, pi = 4.0d0 * datan(1.0d0)

        count_refinement(:) = 0
        count_refinement(0) = n * n

        weight = (/ 4.0d0/9.0d0, 1.0d0/9.0d0, 1.0d0/9.0d0, 1.0d0/9.0d0, 1.0d0/9.0d0, &
                    1.0d0/36.0d0, 1.0d0/36.0d0, 1.0d0/36.0d0, 1.0d0/36.0d0 /)

        ex = (/ 0.0d0, 1.0d0, 0.0d0, -1.0d0, 0.0d0, 1.0d0, -1.0d0, -1.0d0, 1.0d0 /)
        ey = (/ 0.0d0, 0.0d0, 1.0d0, 0.0d0, -1.0d0, 1.0d0, 1.0d0, -1.0d0, -1.0d0 /)

        do i=1,latticeSize
            Hxx(i) = ex(i)**2 - cs**2
            Hxy(i) = ex(i) * ey(i)
            Hyy(i) = ey(i)**2 - cs**2
            Hxyy(i) = Hyy(i) * ex(i)
            Hyxx(i) = Hxx(i) * ey(i)
            Hxxyy(i) = Hxx(i) * Hyy(i)
        end do

        size_of_fluid = 1.0d0 / 10.d0

        do j = 1,n
            do i = 1,n

                Mesh(i,j)%values(phi) = 1.0d0
                Mesh(i,j)%values(ux) = ux0 * (1.0d0 + Mesh(i,j)%values(phi)) / 2.0d0 &
                                    + 0.0d0 * (1.0d0 - Mesh(i,j)%values(phi)) / 2.0d0
                Mesh(i,j)%values(uy) = uy0 * (1.0d0 + Mesh(i,j)%values(phi)) / 2.0d0 &
                                    + 0.0d0 * (1.0d0 - Mesh(i,j)%values(phi)) / 2.0d0
                Mesh(i,j)%values(phi) = - tanh((sqrt((i - n / 2.0) ** 2 + (j - n / 2.0) ** 2) - r) / W)

                ! Mesh(i,j)%values(phi) = tanh(-(abs(j - n / 2.0d0) - n * size_of_fluid) / W)
                ! Mesh(i,j)%values(uy) = uy0 * (1.0d0 + Mesh(i,j)%values(phi)) / 2.0d0 &
                !                      - uy0 * (1.0d0 - Mesh(i,j)%values(phi)) / 2.0d0
                ! Mesh(i,j)%values(ux) = 0.0025 * sin(2 * pi * i / n)
                ! Mesh(i,j)%values(rho) = density_ratio * (1.0d0 + Mesh(i,j)%values(phi)) / 2.0d0 &
                !                                       + (1.0d0 - Mesh(i,j)%values(phi)) / 2.0d0

                Mesh(i,j)%values(p) = 0.0d0
                Mesh(i,j)%values(f0:f8) = Feq(Mesh(i,j))
                Mesh(i,j)%values(f0col:f8col) = Feq(Mesh(i,j))
            end do
        end do

    end subroutine init_values

    pure function Feq(l) result(eq)
        type(Nodes), intent(in), target :: l
        real(real64) :: eq(1:latticeSize)
        real(real64) :: axx, ayy, axy, axyy, ayxx

        axx = l%values(rho) * l%values(ux)**2
        ayy = l%values(rho) * l%values(uy)**2
        axy = l%values(rho) * l%values(ux) * l%values(uy)
        ayxx = axx * l%values(uy) + cs**2 * (rho_0 - l%values(rho)) * l%values(uy)
        axyy = ayy * l%values(ux) + cs**2 * (rho_0 - l%values(rho)) * l%values(ux)

        eq = weight(:) * (l%values(p) / cs**2 +&
        l%values(rho) * l%values(ux) * ex(:) / cs**2 +&
        l%values(rho) * l%values(uy) * ey(:) / cs**2 +&
        (axx * Hxx(:) + ayy * Hyy(:) + 2.0 * axy * Hxy(:)) / (2.0*cs**4) +&
        (ayxx * Hyxx(:) + axyy * Hxyy(:)) / (2.0*cs**6))

    end function Feq

    subroutine do_computation()
        integer :: i, t

        do t = 1, 2 ** highest_refinement

            !$omp parallel do
            do i=1,number_of_leaves

                if (mod(t, 2**(highest_refinement - leaves(i)%p%refinement_level)) .EQ. 0) then

                    call grad(Phi, leaves(i)%p, leaves(i)%p%values(dPhix), leaves(i)%p%values(dPhiy))
                    call leaves(i)%p%update

                    call advection(leaves(i)%p)
                    call AllenCahn(leaves(i)%p)

                    ! call collision(leaves(i)%p)
                    ! call streaming(leaves(i)%p)
                    ! call reconstruction_macro(leaves(i)%p)

                end if

            end do
            !$omp end parallel do

        end do

    end subroutine do_computation

    real(real64) pure function minmod(a, b) result(lim)
        real(real64), intent(in) :: a, b

        lim = min(0.0d0, a*b) * (min(a, b) + max(a, b)) * sign(1.0d0, a)

    end function minmod

    subroutine advection(l)
        type(Nodes), pointer, intent(in) :: l
        real(real64) :: limx, limx_1, limx_2, valueRx_, valueRx_1
        real(real64) :: valueLx_, valueLx_1
        real(real64) :: limy, limy_1, limy_2, valueRy_, valueRy_1
        real(real64) :: valueLy_, valueLy_1
        integer :: ref

        ref = l%refinement_level

        ! 2D MUSCL
        limx = minmod(l%values(phi) - l%w%values(phi), &
        l%e%values(phi) - l%values(phi)) / (2.0d0 ** ref)
        limx_1 = minmod(l%w%values(phi) - l%w%w%values(phi), &
        l%values(phi) - l%w%values(phi)) / (2.0d0 ** ref)
        limx_2 = minmod(l%e%values(phi) - l%values(phi), &
        l%e%e%values(phi) - l%e%values(phi)) / (2.0d0 ** ref)

        limy = minmod(l%values(phi) - l%s%values(phi), &
        l%n%values(phi) - l%values(phi)) / (2.0d0 ** ref)
        limy_1 = minmod(l%s%values(phi) - l%s%s%values(phi), &
        l%values(phi) - l%s%values(phi)) / (2.0d0 ** ref)
        limy_2 = minmod(l%n%values(phi) - l%values(phi), &
        l%n%n%values(phi) - l%n%values(phi)) / (2.0d0 ** ref)

        ! WARNING: not sure if we should take *ref* into account 
        ! in both terms here
        valueRx_ = l%values(phi) + limx * (1.0d0 - l%values(ux)) / 2.0
        valueLx_ = l%values(phi) - limx * (1.0d0 - l%values(ux)) / 2.0
        valueRx_1 = l%w%values(phi) + limx_1 * (1.0d0 - l%w%values(ux)) / 2.0
        valueLx_1 = l%e%values(phi) - limx_2 * (1.0d0 - l%e%values(ux)) / 2.0

        valueRy_ = l%values(phi) + limy * (1.0d0 - l%values(uy)) / 2.0
        valueLy_ = l%values(phi) - limy * (1.0d0 - l%values(uy)) / 2.0
        valueRy_1 = l%s%values(phi) + limy_1 * (1.0d0 - l%s%values(uy)) / 2.0
        valueLy_1 = l%n%values(phi) - limy_2 * (1.0d0 - l%n%values(uy)) / 2.0

        if (l%values(ux) .LE. 0) then
            l%values(phi) = l%values(phi) + l%values(ux) * (valueLx_ - valueLx_1) / 2.0
        else
            l%values(phi) = l%values(phi) - l%values(ux) * (valueRx_ - valueRx_1) / 2.0
        end if

        if (l%values(uy) .LE. 0) then
            l%values(phi) = l%values(phi) + l%values(uy) * (valueLy_ - valueLy_1) / 2.0
        else
            l%values(phi) = l%values(phi) - l%values(uy) * (valueRy_ - valueRy_1) / 2.0
        end if

        ! we update the parent value + ghost children if they exist
        call l%update

    end subroutine advection

    subroutine AllenCahn(l)
        type(Nodes), pointer, intent(in) :: l
        type(Nodes), pointer :: neighbor_a
        real(real64) :: dPhia, dPhib, d2Phi, grad_phix, grad_phiy, norm, tau
        integer :: a, ref

        ref = l%refinement_level

        dPhia = 0.0d0
        dPhib = 0.0d0
        d2Phi = 0.0d0

        do a=1,latticeSize

            call neighbor(l, a, neighbor_a)

            grad_phix = neighbor_a%values(dPhix)
            grad_phiy = neighbor_a%values(dPhiy)

            norm = sqrt(grad_phix**2 + grad_phiy**2) + epsilon

            dPhia = dPhia + weight(a) * ex(a) * grad_phix / norm +&
            weight(a) * ey(a) * grad_phiy / norm

            dPhib = dPhib + weight(a) * ex(a) * neighbor_a%values(phi)**2 * grad_phix / norm +&
            weight(a) * ey(a) * neighbor_a%values(phi)**2 * grad_phiy / norm

            d2Phi = d2Phi + weight(a) * (neighbor_a%values(phi) - l%values(phi))

        end do

        dPhia = dPhia * 2.0d0 ** ref
        dPhib = dPhib * 2.0d0 ** ref
        d2Phi = d2Phi * 2.0d0 ** (ref + 1)

        tau = compute_tau(l)

        l%values(phi) = l%values(phi) + tau * (d2Phi + (dPhib - dPhia) / W)

        call l%update

    end subroutine AllenCahn

    subroutine grad(value, l, grad_x, grad_y)
        type(Nodes), pointer :: l
        integer, intent(in) :: value
        real(real64), intent(out) :: grad_x, grad_y
        integer :: a, ref
        type(Nodes), pointer :: neighbor_a

        ref = l%refinement_level

        grad_x = 0.0d0
        grad_y = 0.0d0

        do a=1,latticeSize

            call neighbor(l, a, neighbor_a)

            grad_x = grad_x + weight(a) * ex(a) * neighbor_a%values(value)
            grad_y = grad_y + weight(a) * ey(a) * neighbor_a%values(value)

        end do

        grad_x = (2.0d0 ** ref) * grad_x / cs**2
        grad_y = (2.0d0 ** ref) * grad_y / cs**2

    end subroutine grad

    function compute_tau(l) result(tau)
        type(Nodes), pointer, intent(in) :: l
        real(real64) :: tau, nu, tau_c
        integer :: ref

        ref = l%refinement_level

        nu = (nu1 * (1.0d0 + l%values(Phi)) / 2.0d0 + nu2 * (1.0d0 - l%values(Phi)) / 2.0d0)

        tau_c = 0.5d0 + l%values(rho) * nu / (rho_0 * cs**2)
        tau = 2**ref * (tau_c - 1/2.0d0) + 1/2.0d0

    end function compute_tau

    function correction(l) result(S)
        type(Nodes), pointer :: l
        real(real64) :: S(1:latticeSize)
        real(real64) :: duxx, duxy, duyx, duyy, dpx, dpy, drhox, drhoy
        integer :: a

        call grad(ux, l, duxx, duxy)
        call grad(uy, l, duyx, duyy)
        call grad(p, l, dpx, dpy)

        drhox = (1.0d0 - 1.0d0 / density_ratio) * dPhix
        drhoy = (1.0d0 - 1.0d0 / density_ratio) * dPhiy

        ! TODO: still lacks temporal derivative of pressure + forcing term
        do a=1,latticeSize
            S(a) = weight(a) * (rho_0 * cs**2 * (duxx + duyy) * (Hxx(a) + Hyy(a)) +&
            2.0d0 * l%values(ux) * dpx * Hxx(a) + 2.0d0 * l%values(uy) * dpy * Hyy(a) +&
            (l%values(ux) * dpy + l%values(uy) * dpx) * Hxy(a) +&
            ((rho_0 - l%values(rho)) * cs**2 * duxx - drhox * cs**2 * l%values(ux) ) * Hxx(a) +&
            ((rho_0 - l%values(rho)) * cs**2 * duyy - drhoy * cs**2 * l%values(uy) ) * Hyy(a)) / (2.0d0 * cs**4)
        end do

    end function correction

    pure function SurfaceTension(l) result(Omega)
        type(Nodes), pointer :: l
        real(real64) :: Omega(1:latticeSize)
        real(real64) :: tau, norm, gradyPhi, gradxPhi
        integer :: a

        gradxPhi = l%values(dPhix)
        gradyPhi = l%values(dPhiy)
        norm = sqrt(gradxPhi ** 2 + gradyPhi ** 2) + epsilon

        do a=1,latticeSize
            Omega(a) = weight(a) * sigma * (- gradyPhi ** 2 * Hxx(a) -&
            gradxPhi ** 2 * Hyy(a) + 2.0d0 * gradxPhi * gradyPhi * Hxy(a)) /&
            (4.0d0 * cs**4 * norm)
        end do
        
    end function SurfaceTension

    subroutine collision(l)
        type(Nodes), pointer, intent(in) :: l
        real(real64) :: axx, ayy, axy, axyy, ayxx, tau, tau_c
        integer :: ref
        real(real64), dimension(1:latticeSize) :: eq, fneq, Omega, S

        ref = l%refinement_level

        eq = Feq(l)

        ! Omega = SurfaceTension(l)
        Omega = 0.0d0

        ! NOTE: we might need to either store this value (let's say we create an array of the shape of leaves)
        ! because we will need it for the macroscopic reconstruction
        ! either we just forget the correction terms in it and we just add the forcing terms
        ! S = correction(l)
        S = 0.0d0

        !RR
        axx = dot_product((l%values(f0:f8) - eq), Hxx)
        ayy = dot_product((l%values(f0:f8) - eq), Hyy)
        axy = dot_product((l%values(f0:f8) - eq), Hxy)

        ayxx = l%values(uy) * axx + 2.0 * l%values(ux) * axy
        axyy = l%values(ux) * ayy + 2.0 * l%values(uy) * axy
        fneq = weight * ((axx * Hxx + 2.0 * axy * Hxy + ayy * Hyy) / (2.0 * cs**4) +&
        (axyy * Hxyy + ayxx * Hyxx) / (2.0 * cs**6) )

        tau_c = 0.5d0 + l%values(rho) * ((nu1 * (1.0d0 + l%values(Phi)) / 2.0d0 +&
        nu2 * (1.0d0 - l%values(Phi)) / 2.0d0)) / (rho_0 * cs**2)

        tau = compute_tau(l)

        fneq = (0.5d0 * tau / tau_c) ** ref * fneq

        ! collision
        l%values(f0col:f8col) = eq + (1.0d0 - 1.0d0 / tau) * fneq + Omega + S / (2**(ref + 1))

        call l%update

    end subroutine collision

    subroutine streaming(l)
        type(Nodes), pointer, intent(in) :: l
        type(Nodes), pointer :: neighbor_a
        integer :: a

        do a=1,latticeSize
            call inv_neighbor(l, a, neighbor_a)
            l%values(f0 + a - 1) = neighbor_a%values(f0col + a - 1)
        end do

        call l%update

    end subroutine streaming

    subroutine reconstruction_macro(l)
        type(Nodes), pointer, intent(in) :: l
        integer :: a
        real(real64) :: hstr(latticeSize)
        type(Nodes), pointer :: neighbor_a

        ! do a=1,latticeSize
        !     call inv_neighbor(l, a, neighbor_a)
        !     hstr(a) = neighbor_a%values(f0col + a - 1) / neighbor_a%values(rho)
        ! end do
        ! l%values(p) = l%values(p) + l%values(rho) * cs**2 *&
        ! c_pressure * ( sum(hstr) - sum(l%values(f0col:f8col)) / l%values(rho) )

        l%values(p) = cs**2 * sum(l%values(f0:f8))

        l%values(rho) = density_ratio * (1.0d0 + l%values(phi)) / 2.0d0 &
        + (1.0d0 - l%values(phi)) / 2.0d0

        l%values(ux) = sum(l%values(f0:f8) * ex) / l%values(rho)
        l%values(uy) = sum(l%values(f0:f8) * ey) / l%values(rho)

        call l%update

    end subroutine reconstruction_macro


end module LBM
