#######################
## PVD LIB VARIABLES
#######################

DIR_INC := include/
DIR_LIB := lib/
DIR_PVD := PVD/
PVD_LIB := libvtr.a
PVD_OBJ := VTK_mod.o \
		   VTR_mod.o
PVD_SRC := $(addprefix $(DIR_PVD), $(patsubst %.o, %.f90, $(PVD_OBJ)))

#######################
## AMR VARIABLES
#######################

LDFLAGS := -I./include -L./lib -lvtr -fopenmp

DIR_SRC    := src/
DIR_BUILD  := build/

AMR_BIN    := AMR
AMR_OBJ :=  variables.o \
			node.o \
			ptr.o \
			meshing.o \
			LBM.o \
			main.o \

#######################
## FLAGS
#######################

F90       := gfortran
F90FLAGS  := -O3 -Wall -g -fbacktrace -ffast-math -march=native -mtune=native -ftree-vectorize -funroll-loops -pedantic -fstack-arrays -frecursive  -fopenmp

# F90       := ifort
# F90FLAGS  := -O3 -xHost -ipo -qopenmp

#######################
## TARGETS
#######################

.SUFFIXES:

all:
	make $(DIR_LIB)$(PVD_LIB)
	make $(DIR_BUILD)$(AMR_BIN)

clean:
	rm -f $(AMR_OBJ)
	rm -f $(patsubst %.o, %.mod, $(AMR_OBJ))
	rm -f *.vtk *.vtr *.pvd

clean_all:
	rm -rf include lib build
	rm -f *.o *.mod *.vtr *.vtk *.pvd

#######################
## PVD TARGETS
#######################

$(DIR_INC)%.o: $(DIR_PVD)%.f90
	$(F90) -c $(F90FLAGS) $<
	@mkdir -p $(DIR_INC)
	@mv $(patsubst %.f90, %.o, $(<:$(DIR_PVD)%=%)) $(DIR_INC)

$(DIR_LIB)$(PVD_LIB): $(addprefix $(DIR_INC), $(PVD_OBJ))
	@mkdir -p $(DIR_LIB)
	ar rc $@ $^
	ranlib $@

#######################
## AMR TARGETS
#######################

%.o: $(DIR_SRC)%.f90
	${F90} -c ${F90FLAGS} $< $(LDFLAGS)

$(DIR_BUILD)$(AMR_BIN): $(AMR_OBJ)
	@mkdir -p build
	$(F90) $^ -o $@ $(LDFLAGS)
